require('dotenv').config();

const httpServer = require('http-server');

const host = process.env.HOST || 'localhost';
const port = process.env.LOCAL_PORT || 8084;

const server = httpServer.createServer({
  root: './', // Serve files from the current directory
});

server.listen(port, host, () => {
  console.log(`Server is running at http://${host}:${port}/`);
});
