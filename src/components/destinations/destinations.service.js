// require('dotenv').config();

// const baseUrl = `${process.env.API_HOST}:${process.env.API_PORT}`;
const baseUrl = 'http://localhost:3000';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNzE1MzAyMDU0LCJleHAiOjE3MTc4OTQwNTR9.VfuBqZSPqg3RAC08A7LLj20WrlEHC21rjplQYPt4OhE';
const httpConfig = {
  headers: {
    'content-type': 'application/json',
    authorization: `Bearer ${token}`,
  },
};

angular.module('myApp')
  .service('DestinationsService', ['$http', function ($http) {

    return {
      getDestination: (destinationId) => {
        return $http.get(`${baseUrl}/v1/destinations/${destinationId}`, httpConfig)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
      listDestinations: (filter, order, itemsPerPage = 10, page = 1) => {
        const config = {
          ...httpConfig,
          params: {
            filter: JSON.stringify(filter),
            order: JSON.stringify(order),
            items_per_page: itemsPerPage,
            page,
          },
        };

        return $http.get(`${baseUrl}/v1/destinations`, config)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
      createDestination: (destination) => {
        const config = {
          ...httpConfig,
        };

        return $http.post(`${baseUrl}/v1/destinations`, destination, config)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
      updateDestination: (destination) => {
        const config = {
          ...httpConfig,
        };

        return $http.patch(`${baseUrl}/v1/destinations/${destination.id}`, destination, config)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
      overwriteDestination: (destination) => {
        const config = {
          ...httpConfig,
        };

        return $http.put(`${baseUrl}/v1/destinations/${destination.id}`, destination, config)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
      deleteDestination: (destinationId, hardDelete = false) => {
        const config = {
          ...httpConfig,
          params: {
            hard_delete: hardDelete,
          }
        };

        return $http.delete(`${baseUrl}/v1/destinations/${destinationId}`, config)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
      recoverDestination: (destinationId) => {
        const config = {
          ...httpConfig,
        };

        return $http.delete(`${baseUrl}/v1/destinations/recover/${destinationId}`, config)
          .then(function (response) {
            return response.data;
          })
          .catch(function (error) {
            throw error;
          });
      },
    };
  }]);
