angular.module('myApp')
  .controller('DestinationsController', ['$scope', '$mdDialog', 'DestinationsService', function ($scope, $mdDialog, DestinationsService) {
    $scope.countryCodes = ['ESP', 'DEU', 'GBR'];
    $scope.types = ['hotel', 'apartment', 'aparthotel', 'hostel', 'pension', 'camping', 'house'];
    $scope.itemsPerPagePossible = [5, 10, 20, 50, 100];

    $scope.destinations = [];
    $scope.totalDestinations = 0;
    $scope.totalPages = 1; // $scope.totalDestinations / $scope.itemsPerPage
    $scope.totalPagesRange = [1]; // $scope.totalDestinations / $scope.itemsPerPage
    $scope.itemsPerPage = 10;
    $scope.page = 1;
    $scope.filter = {
      id: null,
      name: null,
      description: null,
      country_code: null,
      type: null,
    };
    $scope.order = {
      id: null,
      name: null,
      country_code: null,
      type: null,
    }

    $scope.selected = {
      id: null,
      name: null,
      description: null,
      country_code: null,
      type: null,
    };

    $scope.$watch('[totalDestinations, itemsPerPage]', function (newValues, oldValues) {
      $scope.totalPages = Math.ceil(newValues[0] / newValues[1]);

      // if itemsPerPage change, getList
      if (newValues[1] != oldValues[1]) {
        $scope.page = 1;
        $scope.applyFilters();
      }
    });

    $scope.$watch('totalPages', function (newValue, oldValue) {
      $scope.totalPagesRange = [...Array(newValue).keys()];
    });

    $scope.goToPage = (pageNumber) => {
      if (pageNumber == 0 || pageNumber > $scope.totalPages) return;
      $scope.page = pageNumber;
      $scope.applyFilters();
    };

    // Function to apply filters and fetch destinations
    $scope.applyFilters = () => {
      api.listDestinations($scope, DestinationsService);
    };

    $scope.cleanFilters = () => {
      $scope.filter = {
        id: null,
        name: null,
        description: null,
        country_code: null,
        type: null,
      };

      api.listDestinations($scope, DestinationsService);
    };

    $scope.cleanSelected = () => {
      $scope.selected = {
        id: null,
        name: null,
        description: null,
        country_code: null,
        type: null,
      };
    }

    $scope.orderBy = (field) => {
      switch ($scope.order[field]) {
        case null:
          $scope.order[field] = 'ASC';
          break;
        case 'ASC':
          $scope.order[field] = 'DESC';
          break;
        case 'DESC':
        default:
          $scope.order[field] = null;
          break;
      }

      api.listDestinations($scope, DestinationsService);
    };
    
    // Initial fetch of destinations without filters
    api.listDestinations($scope, DestinationsService);


    // Form dialog
    $scope.openDestinationFormDialog = (ev, destination) => {
      if (destination) {
        const { id, name, description, country_code, type } = destination;
        $scope.selected = { id, name, description, country_code, type };
      }

      $mdDialog.show({
        controller: FormDialogController,
        controllerAs: 'formDialogCtrl',
        templateUrl: 'form-dialog.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: { selected: $scope.selected, countryCodes: $scope.countryCodes, types: $scope.types },
        clickOutsideToClose: false,
        escapeToClose: false,
      }).then(function () {
        if (destination) api.updateDestination($scope, DestinationsService);
        else api.createDestination($scope, DestinationsService);
        $scope.cleanSelected();
      }, function () {
        $scope.cleanSelected();
        console.error('Error deleting destination');
      });
    };

    // Delete dialog
    $scope.openRemoveDialog = (ev, destination) => {
      const confirm = $mdDialog.confirm()
        .title('Would you like to delete the destination?')
        .textContent(`Destination ${destination.name} (${destination.id}) will be deleted.`)
        .ariaLabel('Delete dialog')
        .targetEvent(ev)
        .ok('Sure')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function () {
        api.deleteDestination($scope, DestinationsService, destination.id);
      }, function () {});
    };
  }]);

function mapFilter(filter) {
  // result example: [{ field: "name", value: "riu" }, { field: "type", value: "hotel" }]
  return Object.keys(filter).reduce((result, key) => {
    filter[key] = filter[key]?.trim();
    
    if (!!filter[key]) {
      return [...result, { field: key, value: filter[key] }];
    } else {
      return result;
    }
  }, []);
}

function mapOrder(order) {
  // result example: [{ field: "name", type: "DESC" }]
  return Object.keys(order).reduce((result, key) => {
    if (!!order[key]?.trim()) return [...result, { field: key, type: order[key] }];
    else return result;
  }, []);
}

function FormDialogController($scope, $mdDialog, selected, countryCodes, types) {
  $scope.selected = selected;
  $scope.countryCodes = countryCodes;
  $scope.types = types;

  $scope.isValidSelected = false;
  $scope.$watch('selected', function (newValue, oldValue) {
    $scope.isValidSelected = !!(newValue.name?.trim().length && newValue.description?.trim().length && newValue.country_code?.trim().length && newValue.type?.trim().length);
  }, true);

  $scope.hide = function () {
    console.log('hide dialog')
    $mdDialog.hide();
  };

  $scope.cancel = function () {
    console.log('cancel dialog')
    $mdDialog.cancel();
  };

  $scope.save = function () {
    console.log('save dialog')
    $mdDialog.hide(true);
  };
}


/*********************/
/* Service functions */
/*********************/

const api = {
  listDestinations: ($scope, DestinationsService) => {
    const filter = mapFilter($scope.filter);
    const order = mapOrder($scope.order);

    DestinationsService.listDestinations(filter, order, $scope.itemsPerPage, $scope.page)
      .then(function (responseData) {
        $scope.destinations = responseData.data.rows;
        $scope.totalDestinations = responseData.data.total;
      })
      .catch(function (error) {
        console.error('Error fetching destinations:', error);
      });
  },
  createDestination: ($scope, DestinationsService) => {
    DestinationsService.createDestination($scope.selected)
      .then(function (responseData) {
        api.listDestinations($scope, DestinationsService);
      })
      .catch(function (error) {
        console.error('Error creating destination:', error);
      });
  },
  updateDestination: ($scope, DestinationsService) => {
    DestinationsService.updateDestination($scope.selected)
      .then(function (responseData) {
        api.listDestinations($scope, DestinationsService);
      })
      .catch(function (error) {
        console.error('Error creating destination:', error);
      });
  },
  deleteDestination: ($scope, DestinationsService, destinationId) => {
    DestinationsService.deleteDestination(destinationId, true)
      .then(function (responseData) {
        api.listDestinations($scope, DestinationsService);
      })
      .catch(function (error) {
        console.error('Error creating destination:', error);
      });
  },
};