angular.module('myApp')
  .controller('MenuController', ['$scope', '$location', '$mdSidenav', function ($scope, $location, $mdSidenav) {
    var vm = this;

    // Function to navigate to the home view
    vm.goToHome = function () {
      $location.path('/home');
    };

    // Function to navigate to the destinations view
    vm.goToDestinations = function () {
      $location.path('/destinations');
    };

    vm.toggleMenu = function () {
      $mdSidenav('left').toggle();
    };
  }]);
