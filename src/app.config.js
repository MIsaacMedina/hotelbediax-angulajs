angular.module('myApp')
  .config(['$mdThemingProvider', '$routeProvider', function ($mdThemingProvider, $routeProvider) {
    // Angular Material theme configuration
    $mdThemingProvider.theme('default')
      .primaryPalette('indigo')
      .accentPalette('orange')
      .warnPalette('red')
      .backgroundPalette('grey');
    
    // Application routes configuration
    $routeProvider
      .when('/destinations', {
        templateUrl: 'src/components/destinations/destinations.html',
        controller: 'DestinationsController',
        controllerAs: 'destinationsCtrl'
      })
      .when('/', {
        templateUrl: 'src/components/home/home.html',
        controller: 'HomeController',
        controllerAs: 'homeCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
