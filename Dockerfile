# Use a Node.js images to prepare files and dependencies
FROM node:20.13.0

# Container work directory on /app
WORKDIR /app

# Copy package.json and package-lock.json to install dependencies first
COPY package*.json ./

# Install dependencies
RUN npm i

# Copy all app files
COPY . .

# Expose port 8084 to access the application on localhost:8084
EXPOSE 8084

CMD npm start



# # Use an Nginx image as base
# FROM nginx:latest

# # Instala Node.js y npm en la imagen
# RUN apt-get update && \
#   apt-get install -y nodejs npm && \
#   npm install -g npm@latest && \
#   apt-get clean

# # Copy config file into the image
# COPY nginx.conf /etc/nginx/nginx.conf

# # Copy application files into the image
# COPY . /usr/share/nginx/html/hotelbediax

# # Copia el archivo package.json e instala las dependencias
# COPY package*.json /usr/share/nginx/html/hotelbediax/
# RUN cd /usr/share/nginx/html/hotelbediax && npm install

# # Expose port 80 to access the application on localhost:80
# EXPOSE $LOCAL_PORT

# # Run Nginx on container init
# CMD ["nginx", "-g", "daemon off;"]