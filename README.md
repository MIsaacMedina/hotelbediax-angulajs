# Tourist destination management portal AngularJS front

## Description

Simple technical test for AngularJS.

### Statement

HotelBediaX, a new client of FDSA and one of the most important fictional companies in the hotel sector, has the need to completely renew its tourist destination management portal.

The objective of this test is that you, a full stack developer, create a web application for HotelBediaX that operates on destinations.

The final application will consist of several modules, the first of which will be the Destinations module, which is the one you are going to develop. This app will allow the navigation between the different modules, although at the moment there is only this one.

The required operations that must be implemented are the following:
- Create a new destination
- Read all created destinations and their data
- Update their data
- Delete them
- Filtering the data

Those are the minimum required operations, but you are free to add others if you feel that they are useful.

Users have created a first sketch of how the screen should be, but you have creative freedom to add any improvement in both usability and look and feel. Every change to the design that you propose will be valued, as long as the minimum needs are covered.

![Front example](front-example.png "Front example")

### Requirements

- Front-end:
  - SPA (Single Page Application)
  - AngularJS
  - It must be prepared to handle a high amount of destinations (more than
200.000 records).
- Back-end:
  - REST API
  - NodeJS
  - You can mock the database
- Navigation
  - Menu listing the distinct modules
- CRUD
  - 4 basic operations (create, read, update, delete)
- Executable, standalone demo web app

## Run in local (recommended)

1. Open a terminal and go to the proyect path
1. Execute `npm i`
1. Execute `npm start`
1. In your browser go to http://localhost:8084

## Run with Docker (recommended)

Docker has the correct versions of node (20.13.0). You don't need to install it :)

> We are using the por 8084. If you have it in use, change it in '.env' and 'Dockerfile' files

1. Download [Docker Desktop](https://www.docker.com/products/docker-desktop/) and run it
1. Open a terminal and go to the proyect path
1. Execute `docker-compose up --build`
1. In your browser go to http://localhost:8084

**Remember run the API container to get the data!**
